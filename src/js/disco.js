function disco() {
    let xhr = new XMLHttpRequest();

    xhr.open('POST', './admin.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            window.location.reload();
        }
    }
    xhr.send('disco=ok');
}
