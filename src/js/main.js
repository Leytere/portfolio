$(".fancybox").fancybox({
    autoSize        :  false,
    width: '100%',
    height: '100%'
});

function animateCss(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

const $stars = document.getElementsByClassName('star');


window.addEventListener('load', () => setInterval(starShine, 6000));

function starShine() {
	let rand = Math.floor(Math.random() * 24);
	while ($stars[rand].classList.contains('is-transparent')) {
		rand = Math.floor(Math.random() * 24);
	}
	animateCss(`#${$stars[rand].id}`, 'heartBeat');
}

function blobAnim() {
	animateCss('#blob-face', 'bounce');
}
