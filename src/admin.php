<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
session_start();
include 'loginq.php';

if (!$_SESSION['isLogged']) {
    header("Location: login.php");
}

if ($_POST['disco'] == 'ok') {
    $_SESSION = array();
}

$imgChanged = '';
$cvChanged = '';
$skillChanged = '';
$jobChanged = '';
$projectsChanged = '';

if (!empty($_FILES['image'])) {
    $temp = strpos(mime_content_type($_FILES['image']['tmp_name']), 'image');
    if ($temp == 0) {
        move_uploaded_file($_FILES['image']['tmp_name'], "./img/".$_FILES['image']['name']);
        $db = connectDb();
        $db->query("UPDATE firstpart SET img = '".$_FILES['image']['name']."' WHERE id = 1");
        mysqli_close($db);
        $imgChanged = 'Image Modifiée';
    }
}

if (!empty($_FILES['cv'])) {
    move_uploaded_file($_FILES['cv']['tmp_name'], "./cv/".$_FILES['cv']['name']);
    $db = connectDb();
    $db->query("UPDATE firstpart SET cv = '".$_FILES['cv']['name']."' WHERE id = 1");
    mysqli_close($db);
    $cvChanged = 'CV Modifié';
}

if (isset($_POST['skill-lvl']) && isset($_POST['skill-select'])) {
    $db = connectDb();
    $skillLvl = intval($_POST['skill-lvl']);
    switch($_POST['skill-select']) {
    case 0:
        $db->query("UPDATE skills SET html = ".$skillLvl." WHERE id = 1");
        break;

    case 1:
        $db->query("UPDATE skills SET bootstrap = ".$skillLvl." WHERE id = 1");
        break;

    case 2:
        $db->query("UPDATE skills SET symfony = ".$skillLvl." WHERE id = 1");
        break;

    case 3:
        $db->query("UPDATE skills SET php = ".$skillLvl." WHERE id = 1");
        break;

    case 4:
        $db->query("UPDATE skills SET appcliserv = ".$skillLvl." WHERE id = 1");
        break;

    case 5:
        $db->query("UPDATE skills SET javascript = ".$skillLvl." WHERE id = 1");
        break;

    }
    mysqli_close($db);
    $skillChanged = 'Compétence Modifiée';
}


if (!empty($_POST['entreprise']) && !empty($_POST['role-entreprise']) && !empty($_POST['date-emploi'])) {
    $db = connectDb();
    $db->query("INSERT INTO jobs (workplace, job, num) VALUES ('".$_POST['entreprise']."', '".$_POST['role-entreprise']."', '".$_POST['date-emploi']."')");
    mysqli_close($db);
    $jobChanged = 'Xp Pro Ajoutée';
}

if (!empty($_POST['nom-projet']) && !empty($_FILES['image-projet']) && !empty($_POST['path-projet'])) {
    $db = connectDb();
    move_uploaded_file($_FILES['image-projet']['tmp_name'], './img/'.$_FILES['image-projet']['name']);
    $imgName = $_FILES['image-projet']['name'];
    $db->query("INSERT INTO projects (img, title, path) VALUES ('".$imgName."', '".$_POST['nom-projet']."', '".$_POST['path-projet']."')");
    mysqli_close($db);
    $projectsChanged = 'Projet Ajouté';
}

?>

<!DOCTYPE html>
<html lang='fr'>
    <head>
        <meta charset='UTF-8'> 
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap-reboot.min.css'>
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap-grid.min.css'>
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap.min.css'>
        <link rel='stylesheet' href='../node_modules/nes.css/css/nes.min.css'>
        <link rel='stylesheet' href='./css/styleadmin.css'>
        <script type='text/javascript' src='./js/disco.js'></script>
    </head>
    <body>
        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h1>Administration</h1>
                </div>
            </div>
            <div class='row justify-content-center'>
                <p class='mt-3'><?php echo $imgChanged.$cvChanged.$skillChanged.$jobChanged.$projectsChanged ?></p>
            </div>
        </div>

        <div class='separation6'></div>

        <div class='container'>
            <div class='row justify-content-center'>
                <div class="nes-container is-centered is-rounded">
                    <h3 class='mb-5'>Changement Photo</h3>
                    <form enctype="multipart/form-data" name="changePhoto" method="post">
                        <input type='file' name='image' />
                        <input class='nes-btn is-primary' type="submit" name="submit" value="Sauvegarder"/>
                    </form>
                </div>
            </div>
        </div>

        <div class='separation4'></div>

        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h3 class='mb-5'>Changement CV</h3>
                    <form enctype='multipart/form-data' name='changeCV' method='post'>
                        <input type='file' name='cv'>
                        <input class='nes-btn is-primary' type='submit' name='submit' value='Sauvegarder'>
                    </form>
                </div>
            </div>
        </div>

        <div class='separation4'></div>

        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h3 class='mb-5'>Changement Compétences</h3>
                    <form method='post'>
                        <label for="default_select">Compétence :</label>
                        <div class="nes-select mb-5">
                            <select required id="default_select" name='skill-select'>
                                <option value="" disabled selected hidden>Select...</option>
                                <option value="0">Html/Css</option>
                                <option value="1">Bootstrap</option>
                                <option value='2'>Symfony</option>
                                <option value='3'>PHP</option>
                                <option value='4'>App Client/Server</option>
                                <option value='5'>Javascript</option>
                            </select>
                        </div>
                        <p>Niveau</p>
                        <label>
                            <input type="radio" class="nes-radio" name="skill-lvl" value='0' checked />
                            <span>0</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='1'>
                            <span>1</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='2'>
                            <span>2</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='3'>
                            <span>3</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='4'>
                            <span>4</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='5'>
                            <span>5</span>
                        </label>
                        <label>
                            <input type='radio' class='nes-radio' name='skill-lvl' value='6'>
                            <span>6</span>
                        </label>
                        <div class='w-100'></div>
                        <input type='submit' value='Sauvegarder' class='nes-btn is-primary mt-4'>
                    </form>
                </div>
            </div>
        </div>

        <div class='separation4'></div>

        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h3 class='mb-5'>Ajouter Experiences Pro</h3>
                    <form name='changeXP' method='post'>
                        <label for='entreprise'>Nom Entreprise</label>
                        <input type='text' name='entreprise'>
                        <div class='w-100'></div>
                        <label for='role-entreprise'>Rôle&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                        <input type='text' name='role-entreprise'>
                        <div class='w-100'></div>
                        <label for='date-emploi'>Date&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                        <input type='text' name='date-emploi'>
                        <div class='w-100'></div>
                        <input class='mt-5 nes-btn is-primary' type='submit' name='submit' value='Ajouter'>
                    </form>
                </div>
            </div>
        </div>

        <div class='separation4'></div>

        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h3 class='mb-5'>Ajouter Projets</h3>
                    <form name='addProject' method='post' enctype='multipart/form-data'>
                        <label for='nom-projet'>Nom Projet</label>
                        <input type='text' name='nom-projet'>
                        <div class='w-100'></div>
                        <label for='path-projet'>Chemin&nbsp&nbsp&nbsp&nbsp</label>
                        <input type='text' name='path-projet'>
                        <div class='w-100'></div>
                        <label for='image-projet' class='mt-3 mb-3'>Image</label>
                        <div class='w-100'></div>
                        <input type='file' name='image-projet'>
                        <div class='w-100'></div>
                        <input type='submit' value='Ajouter' class='nes-btn is-primary mt-5'>
                    </form>
                </div>
            </div>
        </div>

        <footer class='container-fluid'>
            <div class='row'>
                <a href='./index.php' class='ml-2'>Retour au portfolio.</a>
                <a href='./' class='ml-5' onclick='disco();'>Deconnection</a>
            </div>
        </footer>
    </body>
</html>
