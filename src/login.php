<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include './loginq.php';
session_start();

if (!empty($_POST['username']) && !empty($_POST['pass'])) {
    $username = $_POST['username'];
    $pass = $_POST['pass'];
    $db = connectDb();
    $check = mysqli_query($db, "SELECT username, pass FROM users WHERE username = '$username' AND pass = '$pass'");

    if (mysqli_num_rows($check) >= 1) {
        $_SESSION['isLogged'] = true;
        header("Location: admin.php");
    } else {
        echo 'Wrong Username/Password';
    }

    mysqli_close($db);

}     

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset='UTF-8'> 
        <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap-reboot.min.css'>
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap-grid.min.css'>
        <link rel='stylesheet' href='../node_modules/bootstrap/dist/css/bootstrap.min.css'>
        <link rel='stylesheet' href='../node_modules/nes.css/css/nes.min.css'>
    </head>

    <body>
        <div class='container'>
            <div class='row justify-content-center'>
                <div class='nes-container is-centered is-rounded'>
                    <h3 class='mb-4'>Connection</h3>
                    <form name='login-form' method='post'>
                        <label for='username'>Username</label>
                        <input type='text' name='username'>
                        <label for='pass'>Pass</label>
                        <input type='password' name='pass'>
                        <input type='submit' value='Login'>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
