<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

function htmlSkills($skillLvl, $fs, $ss, $ts) {
    $skillHtml = "";
    if ($skillLvl > 1) {
        $skillHtml .= "<i class='nes-icon star' id='star-".$fs."'></i>"; 
    } else if ($skillLvl == 1) {
        $skillHtml .= "<i class='nes-icon star is-half' id='star-".$fs."'></i>";
    } else {
        $skillHtml .= "<i class='nes-icon star is-transparent' id='star-".$fs."'></i>";
    }
    if ($skillLvl > 3) {
        $skillHtml .= "<i class='nes-icon star ml-2' id='star-".$ss."'></i>";
    } else if ($skillLvl == 3) {
        $skillHtml .= "<i class='nes-icon star is-half ml-2' id='star-".$ss."'></i>";
    } else {
        $skillHtml .= "<i class='nes-icon star is-transparent ml-2' id='star-".$ss."'></i>";
    }
    if ($skillLvl > 5) {
        $skillHtml .= "<i class='nes-icon star ml-2' id='star-".$ts."'></i>";
    } else if ($skillLvl == 5) {
        $skillHtml .= "<i class='nes-icon star is-half ml-2' id='star-".$ts."'></i>";
    } else {
        $skillHtml .= "<i class='nes-icon star is-transparent ml-2' id='star-".$ts."'></i>";
    }
    return $skillHtml;
}

function jobHtmlMaker($jobs) {
    $jobHtml = '';
    foreach($jobs as $val) {       
        $jobHtml .= "<div class='nes-container is-centered col col-lg-3 m-2'>
            <img src='./img/drawing.png' />
            <p>".$val[1]."</p>
            <p>".$val[2]."</p>
            <p>".$val[3]."</p>
            </div>";
    }
    return $jobHtml;
}

function projectsHtmlMaker($data, $pos) {
    $i = 0;
    $projectsBefore = "";
    $projects = "";
    foreach ($data as $val) {
        if ($i != 0) {        
            $projectsBefore .= "<li data-target='#slider-proj' data-slide-to='".$i."' class='bg-primary'></li>"; 
            $projects .= "<div class='carousel-item'>
                <a href='javascript:;' data-fancybox='gallery' data-type='iframe' data-src='".$val[3]."' class='fancybox'>
                <img class='d-block' src='./img/".$val[1]."'>
                </a>
                <div class='carousel-caption d-none d-md-block'>
                <h5>".$val[2]."</h5>
                </div>
                </div>";
        } else {
            $projects .= "<div class='carousel-item active'>
                <a href='javascript:;' data-fancybox='gallery' data-type='iframe' data-src='".$val[3]."' class='fancybox'>
                <img class='d-block' src='./img/".$val[1]."' alt='Crying Baby Slider' />
                </a>
                <div class='carousel-caption d-none d-md-block'>
                <h5>".$val[2]."</h5>
                </div>
                </div>";
            $projectsBefore .= "<li data-target='#slider-proj' data-slide-to='".$i."' class='active bg-primary'></li>";

        } 
        $i++;
    }
    if ($pos == 1) {
        return $projectsBefore;
    } else {
        return $projects;
    }
}

?>
