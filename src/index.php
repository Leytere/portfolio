<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include './loginq.php';
include './skillsMaker.php';
session_start();

$db = connectDb();

$que = $db->query("SELECT * FROM firstpart;");
$data = mysqli_fetch_assoc($que);

$img = $data['img'];
$cv = $data['cv'];

$que = $db->query("SELECT * FROM jobs;");
$data = mysqli_fetch_all($que);

$jobs = jobHtmlMaker($data);

$que = $db->query("SELECT * FROM projects;");
$data = mysqli_fetch_all($que);

$projectsBefore = projectsHtmlMaker($data, 1);
$projects = projectsHtmlMaker($data, 2);

$que = $db->query("SELECT * FROM skills;");
$data = mysqli_fetch_assoc($que);

$html = htmlSkills($data['html'], 1, 2, 3);
$bootstrap = htmlSkills($data['bootstrap'], 4, 5, 6);
$symfony = htmlSkills($data['symfony'], 7, 8, 9);
$php = htmlSkills($data['php'], 10, 11, 12);
$appcliserv = htmlSkills($data['appcliserv'], 13, 14, 15);
$javascript = htmlSkills($data['javascript'], 16, 17, 18);

mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Portfolio Céleste Cinti</title>
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../node_modules/bootstrap/dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="../node_modules/bootstrap/dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../node_modules/nes.css/css/nes.min.css">
    <link rel="stylesheet" type="text/css" href="../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="../node_modules/animate.css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" defer></script>
    <script type="text/javascript" src='../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js'></script>
    <script type="text/javascript" src='./js/main.js'></script>
</head>
<body>

    <div class='container-fluid'>
        <div class='row justify-content-center fixed-top'>
            <div class='messages'>
                <div class='messages-left'>
                    <i class='nes-bcrikko' id='blob-face'>:</i>
                    <div class='nes-balloon from-left' id='navbar'>
                        <a href='#me-anchor' onclick='blobAnim();'>Moi</a>
                        //
                        <a href='#skill-anchor'onclick='blobAnim();'>Compétences</a>
                        //
                        <a href='#proxp-anchor'onclick='blobAnim();'>Expériences Pro</a>
                        //
                        <a href='#project-anchor'onclick='blobAnim();'>Projets</a>
                        //
                        <a href='mailto:cinti.celes@gmail.com'onclick='blobAnim();'>Contact</a>
                        //
                        <a href='./cv/<?php echo $cv ?>' download>CV</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class='anchors' id='me-anchor'></div>
    <header class='container'>
        <div class='row justify-content-center'>
            <img src='./img/<?php echo $img ?>' id='photo' class='rounded-circle' />
        </div>

        <div class='row justify-content-around mt-5'>
            <p>Céleste Cinti - Développement Web</p>
            <div class='w-100'></div>
            <p>En recherche de contrat de professionalisation.</p>
        </div>

        <div class='row justify-content-center mt-4'>
            <a href='./cv/<?php echo $cv ?>' download><button type='button' class='nes-btn is-warning mr-4'>Telecharger CV</button></a>
            <a href='https://gitlab.com/Leytere' target='_blank'><button type='button' class='nes-btn is-warning ml-4'>GitLab</button></a>
        </div>
    </header>

    <div class='separation'></div>

    <div class='anchors' id='skill-anchor'></div>
    <section class='container'>
        <div class='row justify-content-center'>
            <h2>Compétences</h2>
        </div>


        <div class='separation'></div>

        <div class='row'>

            <div class='col-md-4 col-6'>
                <div class='row skills-txt align-items-center star-row'>
                    Html / Css : 
                </div>
                <div class='row mt-2 skills-txt align-items-center star-row'>
                    Bootstrap : 
                </div>
                <div class='row mt-2 skills-txt align-items-center star-row'>
                    Symfony : 
                </div>
            </div>

            <div class='col-md-2 col-6'>
                <div class='row star-row'>
                    <?php echo $html ?>
                </div>
                <div class='row mt-2 star-row'>
                    <?php echo $bootstrap ?>
                </div>
                <div class='row mt-2 star-row'>
                    <?php echo $symfony ?>
                </div>
            </div>

            <div class='col-md-4 col-6'>
                <div class='row skills-txt align-items-center star-row'>
                    PHP : 
                </div>
                <div class='row mt-2 skills-txt align-items-center star-row'>
                    App Client/Serveur : 
                </div>
                <div class='row mt-2 skills-txt align-items-center star-row'>
                    Javascript : 
                </div>
            </div>

            <div class='col-md-2 col-6'>
                <div class='row star-row'>
                    <?php echo $php ?>
                </div>

                <div class='row mt-2 star-row'>
                    <?php echo $appcliserv ?>
                </div>

                <div class='row mt-2 star-row'>
                    <?php echo $javascript ?>
                </div>
            </div>
        </div>
    </section>

    <div class='separation'></div>

    <div class='anchors' id='proxp-anchor'></div>
    <section class='container'>
        <div class='row justify-content-center'>
            <h2>Expériences Professionnelles</h2>
        </div>

        <div class='separation'></div>

        <div class='row justify-content-center'>
            <?php echo  $jobs?>
        </div>
    </section>

    <div class='separation'></div>

    <section class='container'>

        <div class='anchors' id='project-anchor'></div>
        <div class='row justify-content-center'>
            <h2>Projets</h2>
        </div>

        <div class='separation'></div>

        <div class='row justify-content-center' id='carou-row'>

            <div class='carousel slide' data-ride='carousel' id='slider-proj'>

                <ol class='carousel-indicators'>
                    <?php echo $projectsBefore ?>
                </ol>

                <div class='carousel-inner'>
                    <?php echo $projects ?>
                </div>

                <a class='carousel-control-prev' href='#slider-proj' role='button' data-slide='prev'>
                    <span class='carousel-control-prev-icon bg-primary rounded' aria-hidden='true'></span>
                    <span class='sr-only'>Previous</span>
                </a>
                <a class='carousel-control-next' href='#slider-proj' role='button' data-slide='next'>
                    <span class='carousel-control-next-icon bg-primary rounded' aria-hidden='true'></span>
                    <span class='sr-only'>Next</span>
                </a>

            </div>
        </div>
    </section>

    <div class='container'>
        <div class='row justify-content-center' id='error'>
        </div>
    </div>

    <div class='separation'></div>

    <footer class='container-fluid'>
        <div class='row justify-content-between'>
            <a target="_blank" href="https://www.vecteezy.com/" class='ml-3'>Vector Graphics by Vecteezy!</a>
            <a href="./admin.php" class='mr-3'>Admin</a>
        </div>
    </footer>

</body>
</html>


